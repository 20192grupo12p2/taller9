#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
int main() {
  char cmdline[MAXLINE];
  while (1) {

    printf("> ");
    fgets(cmdline, MAXLINE, stdin);

    if (feof(stdin))
      exit(0);

    eval(cmdline);

  }
}
