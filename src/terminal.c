#include <stdio.h> 
#include <stdlib.h> 
#include <sys/wait.h> 
#include <sys/types.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>
#include <fcntl.h>  
#define MAX 100000
#define MAX_ARG 100

int main(){ 
  	char entrada[MAX] = {0};
  	char * args[MAX_ARG];
  	
	while(strcmp(entrada,"exit") != 0){
  		printf("$ ");
		fgets(entrada, MAX, stdin);
		entrada[strcspn(entrada, "\n")] = 0;
		char *p = strtok(entrada," ");
		int i = 0;

		while (p != NULL){
    		args[i] = p;
    		p = strtok (NULL, " ");
    		i++;
  		}

  		pid_t pid = fork();				// Se crea una copia de la terminal

		if(strcmp(entrada,"exit") != 0){
			if (pid < 0) {
				perror("Error en el fork");
				exit(1);
			}else if (pid == 0){
				execv(args[0],args);		// se ejecuta el programa que se llama
				perror("Error en execv");	// Si retorna el execv entonces es un error
				exit(-1);

			}else if(pid>0){
				wait(NULL);					//Espera que termine el programa que se ejecuta por execv
			}
		}
		
	}
    return 0; 
} 