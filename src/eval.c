#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define UNIXERR_H_INCLUDED
#include <string.h>
void eval(char *cmdline){
  char *argv[MAXARGS];
  char buf[MAXLINE];
  extern char **environ;
  int bg;
  pid_t pid;
  strcpy(buf, cmdline);
  bg = parseline(buf, argv);
  if (argv[0] == NULL)
    return;
  if (!builtin_command(argv)) {
    if ((pid = fork()) == 0) {
      if (execve(argv[0], argv, environ) < 0) {
        printf("%s: Command not found.\n", argv[0]);
        exit(0);
      }
    }


    if (!bg) {
      int status;
      if (waitpid(pid, &status, 0) < 0)
        perror("waitfg: waitpid error");
    }
    else
    printf("%d %s", pid, cmdline);
  }
  return;
}

int builtin_command(char **argv){
  if (!strcmp(argv[0], "quit"))
    exit(0);
  if (!strcmp(argv[0], "&"))
    return 1;
  return 0;
}
