bin/terminal: obj/terminal.o
	gcc -Wall -g obj/terminal.o -o bin/terminal

obj/terminal.o: src/terminal.c
	gcc -Wall -g -c src/terminal.c -o obj/terminal.o 

.PHONY: run
run:
	bin/terminal
	
.PHONY: clean
clean:
	rm bin/* obj/*
