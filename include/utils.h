
#define MAXARGS 128
#define MAXLINE 128

void eval(char *cmdline);
int parseline(char *buf, char **argv);
int builtin_command(char **argv);
